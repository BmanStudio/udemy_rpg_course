﻿using BornToSit.Combat;
using BornToSit.Core;
using BornToSit.Movement;
using BornToSit.Resources;
using UnityEngine;

namespace BornToSit.Control
{
    public class AIController : MonoBehaviour
    {
        [Header("Sight ranges:")]
        [SerializeField] float sightRangeDefault = 5f;
        [SerializeField] float suspiciousSightRange = 8f;

        [SerializeField] float suspiciousTime = 5f;

        [Header("Patrol behaviour:")]
        [SerializeField] PatrolPath patrolPath;
        [SerializeField] float waypointTolerance = 2f;
        [SerializeField] float dwellInWaypointTime = 1.5f;
        [Range(0, 1)] [SerializeField] float patrolSpeedFracion = 0.53f;

        float timeSinceLastSawPlayer = Mathf.Infinity;
        float timeSinceArrivedAtWaypoint = Mathf.Infinity;
        Vector3 guardPosition;
        int currentWaypointIndex = 0;
        float sightDistance;

        GameObject player;
        Fighter fighter;
        Health health;
        Mover mover;
        Weapon weapon;
        private void Start()
        {
            player = GameObject.FindWithTag("Player");
            fighter = GetComponent<Fighter>();
            health = GetComponent<Health>();
            mover = GetComponent<Mover>();

            guardPosition = transform.position;

        }
        private void Update()
        {
            if (health.GetIsDead()) { return; }
            if (player == null) { return; }

            if (InAttackRangeOfPlayer() && fighter.CanAttackTarget(player))
            {
                ChaseAndAttack();
            }
            else if (PlayerInSuspiciousRangeAndAttackable())
            {
                SuspiciousBehaviour();
            }
            else
            {
                PatrolBehaviour();
            }
            UpdateTimers();
        }

        private void UpdateTimers()
        {
            timeSinceLastSawPlayer += Time.deltaTime;
            timeSinceArrivedAtWaypoint += Time.deltaTime;
        }

        public void UpdateSightDistance()
        {
            if (fighter.GetCurrentWeapon() != null)
            {
                weapon = fighter.GetCurrentWeapon();

                if (weapon.weaponRange > sightDistance)
                {
                    sightDistance = weapon.weaponRange;
                }
                else if (sightDistance != sightRangeDefault && weapon.weaponRange <= sightRangeDefault)
                {
                    sightDistance = sightRangeDefault;
                }
            }
        }

        private bool PlayerInSuspiciousRangeAndAttackable()
        {
            return !InAttackRangeOfPlayer() && timeSinceLastSawPlayer < suspiciousTime && fighter.CanAttackTarget(player);
        }

        private void PatrolBehaviour()
        {
            Vector3 nextPosition = guardPosition;

            if (patrolPath != null)
            {
                if (AtWaypoint())
                {
                    timeSinceArrivedAtWaypoint = 0;
                    CycleWaypoint();
                }

                nextPosition = GetCurrentWaypoint();
            }

            if (timeSinceArrivedAtWaypoint > dwellInWaypointTime)
            {
                mover.StartMoveAction(nextPosition, patrolSpeedFracion);

            }
        }

        private Vector3 GetCurrentWaypoint()
        {
            return patrolPath.GetWaypoint(currentWaypointIndex);
        }

        private void CycleWaypoint()
        {
            currentWaypointIndex = patrolPath.GetNextWaypoint(currentWaypointIndex);
        }

        private bool AtWaypoint()
        {
            float distanceToWaypoint = Vector3.Distance(transform.position, GetCurrentWaypoint());
            return distanceToWaypoint < waypointTolerance;
        }

        private void SuspiciousBehaviour() // if the player stays near the stop location of the AI, he will still wait there and look at the player
        {
            GetComponent<ActionScheduler>().CancelCurrentAction();
            if (InSuspiciousSightRange())
            {
                transform.LookAt(player.transform);
                timeSinceLastSawPlayer = 0;
            }
        }

        private void ChaseAndAttack()
        {
            timeSinceLastSawPlayer = 0;
            fighter.Attack(player);
        }

        private bool InAttackRangeOfPlayer()
        {
            float distanceToPlayer = Vector3.Distance(transform.position, player.transform.position);
            UpdateSightDistance();
            return distanceToPlayer <= sightDistance;
        }

        private bool InSuspiciousSightRange()
        {
            return Vector3.Distance(transform.position, player.transform.position) < suspiciousSightRange;
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, sightDistance); // chase gizmos
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(transform.position, suspiciousSightRange); // suspicious gizmos
        }
    }
}
