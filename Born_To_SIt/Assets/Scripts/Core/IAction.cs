﻿namespace BornToSit.Core
{
    public interface IAction
    {
        void Cancel();
    }
}