﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BornToSit.Saving
{
    public class SavingSystem : MonoBehaviour
    {
        public float fadeOutTime = 0.1f;
        public float fadeInTime = 1.5f;
        public IEnumerator LoadLastSavedScene(string saveFileName)
        {
            Dictionary<string, object> stateDict = LoadFile(saveFileName);

            if (stateDict.ContainsKey("lastSceneIndex"))
            {
                int lastSceneIndex = (int)stateDict["lastSceneIndex"];
                if (lastSceneIndex != SceneManager.GetActiveScene().buildIndex)
                {
                    yield return SceneManager.LoadSceneAsync(lastSceneIndex);
                }
            }
            RestoreState(stateDict);
        }
        public void Save(string saveFileName)
        {
            Dictionary<string, object> state = LoadFile(saveFileName);
            CaptureState(state);
            Savefile(saveFileName, state);
        }


        public void Load(string saveFileName)
        {
            RestoreState(LoadFile(saveFileName));
        }


        private void RestoreState(Dictionary<string, object> state)
        {
            foreach (SaveableEntity saveable in FindObjectsOfType<SaveableEntity>())
            {
                string id = saveable.GetUniqueIdentifier();
                if (state.ContainsKey(id))
                {
                    saveable.RestoreEntityState(state[id]);
                }
            }
        }

        private void CaptureState(Dictionary<string, object> state)
        {
            foreach (SaveableEntity saveable in FindObjectsOfType<SaveableEntity>())
            {
                state[saveable.GetUniqueIdentifier()] = saveable.CaptureEntityState();
            }

            state["lastSceneIndex"] = SceneManager.GetActiveScene().buildIndex;
        }

        private void Savefile(string saveFileName, object state)
        {
            string path = GetPathFromSaveFile(saveFileName);
            print("Saving to " + path);
            using (FileStream stream = File.Open(path, FileMode.Create))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, state);
            }
        }

        private Dictionary<string, object> LoadFile(string saveFileName)
        {
            string path = GetPathFromSaveFile(saveFileName);
            if (!File.Exists(path))
            {
                return new Dictionary<string, object>();
            }
            using (FileStream savedFile = File.Open(path, FileMode.Open))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                return (Dictionary<string, object>)formatter.Deserialize(savedFile);
            }
        }

        private string GetPathFromSaveFile(string saveFile)
        {
            return Path.Combine(Application.persistentDataPath, saveFile + ".sav");
        }
    }
}
