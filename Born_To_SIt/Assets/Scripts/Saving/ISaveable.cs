﻿namespace BornToSit.Saving
{
    public interface ISaveable
    {
        object CaptureEntityState();
        void RestoreEntityState(object state);
    }
}