﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace BornToSit.Saving
{
    [ExecuteAlways]
    public class SaveableEntity : MonoBehaviour
    {
        [SerializeField] string uniqueIdentifier = "";
        static Dictionary<string, SaveableEntity> globalUGIDLookup = new Dictionary<string, SaveableEntity>();
        public string GetUniqueIdentifier()
        {
            return uniqueIdentifier;
        }

        public object CaptureEntityState()
        {
            Dictionary<string, object> state = new Dictionary<string, object>();
            foreach (ISaveable saveable in GetComponents<ISaveable>())
            {
                state[saveable.GetType().ToString()] = saveable.CaptureEntityState();
            }
            return state;
        }

        public void RestoreEntityState(object state)
        {
            Dictionary<string, object> stateDict = (Dictionary<string, object>)state;
            foreach (ISaveable saveable in GetComponents<ISaveable>())
            {
                string typeString = saveable.GetType().ToString();
                if (stateDict.ContainsKey(typeString))
                {
                    saveable.RestoreEntityState(stateDict[typeString]);
                }
            }
        }

#if UNITY_EDITOR
        private void Update()
        {
            if (Application.IsPlaying(gameObject)) { return; }
            if (string.IsNullOrEmpty(gameObject.scene.path)) { return; }

            GenerateGUID();
        }

        private void GenerateGUID()
        {
            SerializedObject serializedObject = new SerializedObject(this);
            SerializedProperty property = serializedObject.FindProperty("uniqueIdentifier");
            if (string.IsNullOrEmpty(property.stringValue) || !IsUGIDUnique(property.stringValue))
            {
                property.stringValue = System.Guid.NewGuid().ToString();
                serializedObject.ApplyModifiedProperties();
            }
            globalUGIDLookup[property.stringValue] = this;
        }
#endif

        private bool IsUGIDUnique(string candidate)
        {
            if (!globalUGIDLookup.ContainsKey(candidate)) { return true; }

            if (globalUGIDLookup[candidate] == this) { return true; } // if the UGID is already exsists AND it's not this entity own UGID - this UGID already exsists for someone else

            if (globalUGIDLookup[candidate] == null) // for a case where moving between scenes and the gameobjects are deleted but the dictionary still contains the old UGID
            {
                globalUGIDLookup.Remove(candidate);
                return true;
            }

            if (globalUGIDLookup[candidate].GetUniqueIdentifier() != candidate) // for a case of wierd bug
            {
                globalUGIDLookup.Remove(candidate);
                return true;
            }

            else { return false; }
        }
    }
}