﻿using BornToSit.Core;
using BornToSit.Resources;
using BornToSit.Saving;
using UnityEngine;
using UnityEngine.AI;

namespace BornToSit.Movement
{
    public class Mover : MonoBehaviour, IAction, ISaveable
    {
        [SerializeField] float maxSpeed = 5.6f;
        NavMeshAgent navMeshAgent;
        Health health;

        private void Start()
        {
            navMeshAgent = GetComponent<NavMeshAgent>();
            health = GetComponent<Health>();
        }
        void Update()
        {
            navMeshAgent.enabled = !health.GetIsDead();
            UpdateAnimator();
        }

        private void UpdateAnimator()
        {
            var playerCurrentVelocity = navMeshAgent.velocity;
            Vector3 localVelocity = transform.InverseTransformDirection(playerCurrentVelocity);
            float velocitySpeed = localVelocity.z;
            GetComponent<Animator>().SetFloat("forwardSpeed", velocitySpeed);
        }

        public void StartMoveAction(Vector3 destination, float speedFraction)
        {
            GetComponent<ActionScheduler>().StartAction(this);
            MoveTo(destination, speedFraction);
        }

        public void MoveTo(Vector3 destination, float speedFraction)
        {
            navMeshAgent.isStopped = false;
            navMeshAgent.speed = maxSpeed * Mathf.Clamp01(speedFraction);
            navMeshAgent.destination = destination;
        }

        public void Cancel()
        {
            navMeshAgent.isStopped = true;
        }

        public object CaptureEntityState()
        {
            return new SerializableVector3(transform.position);
        }

        public void RestoreEntityState(object state)
        {
            SerializableVector3 savedPosition = (SerializableVector3)state;
            GetComponent<NavMeshAgent>().enabled = false; // to handle navmeshagent glitch
            transform.position = savedPosition.ToVector();
            GetComponent<NavMeshAgent>().enabled = true; // like that ^^
            if (GetComponent<ActionScheduler>() != null)
            {
                GetComponent<ActionScheduler>().CancelCurrentAction(); // to handle the action after loading bug
            }
        }
    }
}