﻿using System.Collections;
using BornToSit.Saving;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

namespace BornToSit.SceneManagement
{
    enum DestinationIdentifier
    {
        Start,
        End
    }
    public class Portal : MonoBehaviour
    {
        [SerializeField] float fadeOutTime = 2f;
        [SerializeField] float fadeInTime = 1f;

        [SerializeField] int sceneIndexToLoad = 0;
        [SerializeField] Transform spawnPoint;
        [SerializeField] DestinationIdentifier destination;

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Player")
            {
                StartCoroutine(Transition());
            }
        }

        private IEnumerator Transition()
        {
            Fader fader = FindObjectOfType<Fader>();
            SavingWrapper savingWrapper = FindObjectOfType<SavingWrapper>();

            DontDestroyOnLoad(gameObject);

            yield return fader.FadeOut(fadeOutTime);

            savingWrapper.SaveByWrapper();

            yield return SceneManager.LoadSceneAsync(sceneIndexToLoad);
            savingWrapper.LoadByWrapper();

            Portal otherPortal = GetOtherPortal();

            UpdatePlayerPos(otherPortal);

            savingWrapper.SaveByWrapper();

            yield return fader.FadeIn(fadeInTime);

            Destroy(gameObject);
        }

        private void UpdatePlayerPos(Portal otherPortal)
        {
            GameObject player = GameObject.FindWithTag("Player");
            player.GetComponent<NavMeshAgent>().enabled = false; // to handle navmeshagent glitch
            player.transform.position = otherPortal.spawnPoint.position;
            player.transform.rotation = otherPortal.spawnPoint.rotation;
            player.GetComponent<NavMeshAgent>().enabled = true; // to handle navmeshagent glitch
        }

        private Portal GetOtherPortal()
        {
            foreach (Portal portal in FindObjectsOfType<Portal>())
            {
                if (portal == this) { continue; }
                if (portal.destination != destination) { continue; }
                return portal;
            }
            return null;

        }
    }
}
