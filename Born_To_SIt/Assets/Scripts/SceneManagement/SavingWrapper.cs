﻿using System.Collections;
using BornToSit.Saving;
using UnityEngine;

namespace BornToSit.SceneManagement
{
    public class SavingWrapper : MonoBehaviour
    {
        const string defaultSaveFile = "save";

        public float fadeInTime = 1f;
        private IEnumerator Start()
        {
            Fader fader = FindObjectOfType<Fader>();

            fader.FadeOutImmediate();

            yield return GetComponent<SavingSystem>().LoadLastSavedScene(defaultSaveFile);

            yield return fader.FadeIn(fadeInTime);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                SaveByWrapper();
            }

            if (Input.GetKeyDown(KeyCode.L))
            {
                LoadByWrapper();
            }
        }

        public void LoadByWrapper()
        {
            GetComponent<SavingSystem>().Load(defaultSaveFile);
        }

        public void SaveByWrapper()
        {
            GetComponent<SavingSystem>().Save(defaultSaveFile);
        }
    }
}
