﻿namespace BornToSit.Stats
{
    public enum CharacterClass
    {
        Player,
        Grunt,
        Mage,
        Archer
    }
}