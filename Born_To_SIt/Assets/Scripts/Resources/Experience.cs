﻿using BornToSit.Saving;
using UnityEngine;

namespace BornToSit.Resources
{
    public class Experience : MonoBehaviour , ISaveable
    {
        [SerializeField] float experiencePoint = 0f;

        public void GainXP(float exPoint)
        {
            experiencePoint += exPoint;
        }

        public float GetExPoints()
        {
            return experiencePoint;
        }


        public object CaptureEntityState()
        {
            return experiencePoint;
        }
        public void RestoreEntityState(object state)
        {
            experiencePoint = (float)state;
        }
    }
}