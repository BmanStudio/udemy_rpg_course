﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BornToSit.Resources
{
    public class ExpDisplay : MonoBehaviour
    {
        Experience experience;
        Text text;
        private void Awake()
        {
            experience = GameObject.FindWithTag("Player").GetComponent<Experience>();
            text = GetComponent<Text>();
        }

        void Update()
        {
            text.text = experience.GetExPoints().ToString();
        }
    }
}
