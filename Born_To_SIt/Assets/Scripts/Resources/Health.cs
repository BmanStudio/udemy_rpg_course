﻿using System;
using BornToSit.Core;
using BornToSit.Saving;
using BornToSit.Stats;
using UnityEngine;

namespace BornToSit.Resources
{
    public class Health : MonoBehaviour, ISaveable
    {
        public bool isDead = false;

        int healthPoints = 100;

        private void Awake()
        {
            healthPoints = (int)GetComponent<BaseStats>().GetBaseStat(Stat.HealthPoints);
        }
        public bool GetIsDead()
        {
            return isDead;
        }

        public int GetHealthPoint()
        {
            return healthPoints;
        }
        public void TakeDamage(int damage, GameObject instigator)
        {
            if (healthPoints > 0 && !isDead)
            {
                healthPoints -= damage;
                if (healthPoints <= 0)
                {
                    DeathSequence();
                    AwardExperience(instigator);
                }
            }
        }

        private void DeathSequence()
        {
            if (isDead) { return; }

            GetComponent<Animator>().SetTrigger("death");
            GetComponent<ActionScheduler>().CancelCurrentAction();
            isDead = true;
        }

        private void AwardExperience(GameObject instigator)
        {
            Experience experience = instigator.GetComponent<Experience>();
            if (experience == null) { return; }

            experience.GainXP(GetComponent<BaseStats>().GetBaseStat(Stat.ExperienceReward));
        }

        public object CaptureEntityState()
        {
            return healthPoints;
        }

        public void RestoreEntityState(object state)
        {
            healthPoints = (int)state;
            if (healthPoints <= 0)
            {
                DeathSequence();
            }
        }
    }
}
