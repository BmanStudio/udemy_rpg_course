﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BornToSit.Resources
{
    public class HealthDisplay : MonoBehaviour
    {
        Health health;
        Text text;
        private void Awake()
        {
            health = GameObject.FindWithTag("Player").GetComponent<Health>();
            text = GetComponent<Text>();
        }

        void Update()
        {
            text.text = health.GetHealthPoint().ToString();
        }
    }
}
