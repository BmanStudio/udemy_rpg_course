﻿using BornToSit.Core;
using BornToSit.Movement;
using BornToSit.Resources;
using BornToSit.Saving;
using UnityEngine;

namespace BornToSit.Combat
{
    public class Fighter : MonoBehaviour, IAction, ISaveable
    {
        #region variables

        [SerializeField] Transform rightHandTransform = null;
        [SerializeField] Transform leftHandTransform = null;

        [SerializeField] Weapon defaultWeapon = null;

        [Range(0, 1)] [SerializeField] float runToTargetSpeedFraction = 0.85f;

        Weapon currentWeapon;

        float timeSinceLastAttack = Mathf.Infinity;

        Health target;

        #endregion

        private void Start()
        {
            if (currentWeapon == null)
            {
                EquipWeapon(defaultWeapon);
            }
        }


        private void Update()
        {
            timeSinceLastAttack += Time.deltaTime;
            if (target == null) { return; }
            if (target.GetIsDead()) { return; }
            if (!GetIsInRange())
            {
                GetComponent<Mover>().MoveTo(target.transform.position, runToTargetSpeedFraction);
            }
            else
            {
                GetComponent<Mover>().Cancel();
                AttackBehaviour();
            }
        }
        public bool CanAttackTarget(GameObject combatTarget)
        {
            if (combatTarget == null) { return false; }
            Health targetToTest = combatTarget.GetComponent<Health>();
            return targetToTest != null && !targetToTest.GetIsDead();
        }

        private bool GetIsInRange()
        {
            return Vector3.Distance(transform.position, target.transform.position) < currentWeapon.GetWeaponRange();
        }

        public void Attack(GameObject combatTarget) // called from PlayerController. sets the target.
        {
            GetComponent<ActionScheduler>().StartAction(this);
            target = combatTarget.GetComponent<Health>();
            GetComponent<Animator>().ResetTrigger("stopAttack"); // prevent animation glitch bug
        }

        private void AttackBehaviour()
        {
            transform.LookAt(target.transform.position);
            if (timeSinceLastAttack >= currentWeapon.GetTimeBetweenAttacks())
            {
                GetComponent<Animator>().SetTrigger("attack");
                // this is the time that the trigger Hit() event is triggered by the animation, which makes the damage.
                timeSinceLastAttack = 0;
            }
        }

        private void StopAttackAnimation()
        {
            GetComponent<Animator>().ResetTrigger("attack");
            GetComponent<Animator>().SetTrigger("stopAttack");
        }

        void Hit() // Animation event called from the Animator, Attack animation

        {
            if (target == null) { return; }
            if (currentWeapon.GetHasProjectile())
            {
                currentWeapon.LaunchProjectile(rightHandTransform, leftHandTransform, target, gameObject);
            }
            else
            {
                target.TakeDamage(currentWeapon.GetWeaponDamage(), gameObject);
            }
        }

        void Shoot() // Animation event called from the Animator, bow shooting animation.
        {
            Hit();
        }


        public void EquipWeapon(Weapon weapon)
        {
            currentWeapon = weapon;
            Animator animator = GetComponent<Animator>();
            currentWeapon.Spawn(rightHandTransform, leftHandTransform, animator);
        }

        public void Cancel()
        {
            StopAttackAnimation();
            target = null;
            GetComponent<Mover>().Cancel();
        }

        public Weapon GetCurrentWeapon()
        {
            return currentWeapon;
        }

        public Health GetTarget()
        {
            return target;
        }

        public object CaptureEntityState()
        {
            if (currentWeapon == null)
            {
                return defaultWeapon.name;
            }
            else
            {
                return currentWeapon.name;
            }
        }

        public void RestoreEntityState(object state)
        {
            string weaponName = (string)state;
            Weapon weapon = UnityEngine.Resources.Load<Weapon>(weaponName);
            EquipWeapon(weapon);
        }
    }
}
