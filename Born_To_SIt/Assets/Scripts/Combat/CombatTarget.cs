﻿using BornToSit.Resources;
using UnityEngine;


namespace BornToSit.Combat
{
    [RequireComponent(typeof(Health))]
    public class CombatTarget : MonoBehaviour
    {
        // This script exists because the PlayerController.cs is looking for CombatTarget component on a gameobject to know if it's an enemy or not
    }
}