﻿using System.Collections;
using UnityEngine;

namespace BornToSit.Combat
{
    public class WeaponPickup : MonoBehaviour
    {
        [SerializeField] Weapon weapon = null;
        [SerializeField] float respawnTime = 5f;
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "Player")
            {
                EquipTheWeapon(other);
                StartCoroutine(HideForSeconds(respawnTime));
            }
        }

        private void EquipTheWeapon(Collider player)
        {
            Fighter fighter = player.GetComponent<Fighter>();
            fighter.EquipWeapon(weapon);
        }

        private IEnumerator HideForSeconds(float seconds)
        {
            HidePickup();
            yield return new WaitForSeconds(seconds);
            ShowPickup();
        }

        private void ShowPickup()
        {
            ToggleCapsuleCollider();
            foreach (Transform child in transform)
            {
                child.gameObject.SetActive(true);
            }
        }

        private void HidePickup()
        {
            ToggleCapsuleCollider();
            foreach (Transform child in transform)
            {
                child.gameObject.SetActive(false);
            }
        }

        private void ToggleCapsuleCollider()
        {
            CapsuleCollider capsuleCollider = GetComponent<CapsuleCollider>();
            capsuleCollider.enabled = !capsuleCollider.enabled;
        }
    }
}
