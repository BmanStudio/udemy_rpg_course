﻿using BornToSit.Core;
using BornToSit.Resources;
using UnityEngine;

namespace BornToSit.Combat
{
    public class Projectile : MonoBehaviour
    {
        [Header("Combat Settings")]
        [SerializeField] float speed = 20f;
        [SerializeField] int damage = 0;
        [SerializeField] bool isHoming = false;

        [Header("Self Destruction Settings")]
        [SerializeField] bool selfDestruction = true;
        [SerializeField] float selfDestroyTimer = 10f;
        [SerializeField] float destryTimeAfterImpact = 1f;

        [Header("Impact VFX Settings")]
        [SerializeField] GameObject hitFX = null;
        [SerializeField] float destryHitFXTimer = 1f;
        [SerializeField] GameObject[] destryOnHitByParts = null;

        private bool isShoted = false;

        Health target = null;

        Vector3 lastAimDirection;

        GameObject instigator = null;

        private void Start()
        {
            transform.LookAt(GetAimLocation());
            if (selfDestruction)
            {
                Invoke("SelfDestroy", selfDestroyTimer);
            }
        }
        private void Update()
        {
            if (target == null) { return; }

            if (isShoted == false || isHoming == true)
            {
                if (!target.GetIsDead())
                {
                    transform.LookAt(GetAimLocation());
                }
            }
            transform.Translate(Vector3.forward * speed * Time.deltaTime);
        }

        public void SetTarget(Health target, int launcherDamage, GameObject instigator)
        {
            this.target = target;
            this.damage += launcherDamage;
            this.instigator = instigator;
        }
        private Vector3 GetAimLocation()
        {
                if (target.GetComponent<CapsuleCollider>() == null) { return target.transform.position; }
                CapsuleCollider targetCapsule = target.GetComponent<CapsuleCollider>();
                lastAimDirection = target.transform.position + Vector3.up * targetCapsule.height * 3 / 5;
                isShoted = true;

                return lastAimDirection;
        }

        private void OnTriggerEnter(Collider other)
        {
            Health otherHealth = other.GetComponent<Health>();
            if (otherHealth.GetIsDead()) { return; }
            if (otherHealth != target) // todo can also hit other GameObjects with health, such as other enemies or NPC
            {
                return;
            }
            otherHealth.TakeDamage(damage, instigator);

            speed = 0;
            if (hitFX != null)
            {
                GameObject vFX = Instantiate(hitFX, GetAimLocation(), transform.rotation);
                Destroy(vFX, destryHitFXTimer);
            }
            if (selfDestruction)
            {
                SelfDestroy();
            }
        }

        private void SelfDestroy()
        {
            foreach (GameObject destroyItem in destryOnHitByParts)
            {
                Destroy(destroyItem);
            }

            Destroy(gameObject, destryTimeAfterImpact);
        }
    }
}