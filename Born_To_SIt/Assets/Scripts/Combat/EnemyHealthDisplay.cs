﻿using BornToSit.Resources;
using UnityEngine;
using UnityEngine.UI;

namespace BornToSit.Combat
{
    public class EnemyHealthDisplay : MonoBehaviour
    {
        Health targetHealth;
        Fighter fighter;
        Text text;
        private void Awake()
        {
            fighter = GameObject.FindWithTag("Player").GetComponent<Fighter>();
            text = GetComponent<Text>();
        }

        void Update()
        {
            targetHealth = fighter.GetTarget();
            if (targetHealth != null)
            {
                text.text = targetHealth.GetHealthPoint().ToString();
            }
            else
            {
                text.text = "N/A";
            }
        }
    }
}
